<?php
	/**
	 * Project: UberCube
	 * License: CC BY-NC-SA All �All right reserved
	 */

//Start session
	session_start();

//Path config
	define("RACINE", __DIR__);

//Database config
	define("DB_HOST", "localhost");
	define("DB_USERNAME", "root");
	define("DB_PASSWORD", "");
	define("DB_NAME", "ubercube");

//User param
	define("USER_NAME_MINLENGTH", 3);
	define("USER_NAME_MAXLENGTH", 15);
	define("USER_PASSWORD_MINLENGTH", 4);
	define("USER_PASSWORD_MAXLENGTH", 8);
	define("USER_PASSWORD_SALTPREFIX", "Asui8@@as#");
	define("USER_PASSWORD_SALTSUFFIX", "BDipo92##4");
