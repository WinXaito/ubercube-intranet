<?php
	/**
	 * Project: UberCube
	 */

	/**
	 * Class User
	 */
	class User{
		/**
		 * @var $_username string
		 * @var $_password string
		 * @var $_id int
		 * @var $_email string
		 * @var $_firstName string
		 * @var $_lastName string
		 * @var $_rank int
		 */
		private $_username;
		private $_password;
		private $_id;
		private $_email;
		private $_firstName;
		private $_lastName;
		private $_rank;

		/**
		 * @param $username
		 * @param $password
		 * @param $email
		 * @param $firstName
		 * @param $lastName
		 * @param $rank
		 */
		public function setUser($username, $password, $email, $firstName, $lastName, $rank){
			$this->_username = $username;
			$this->_password = $password;
			$this->_email = $email;
			$this->_firstName = $firstName;
			$this->_lastName = $lastName;
			$this->_rank = $rank;
		}

		/**
		 * @return $this
		 */
		public function getUser(){
			return $this;
		}

		/**
		 * @return mixed
		 */
		public function getUserName(){
			return $this->_username;
		}

		/**
		 * @param $username
		 */
		public function setUserName($username){
			$this->_username = $username;
		}

		/**
		 * @return mixed
		 */
		public function getPassword(){
			return $this->_password;
		}

		/**
		 * @param $password
		 */
		public function setPassword($password){
			$this->_password = $password;
		}

		/**
		 * @return mixed
		 */
		public function getId(){
			return $this->_id;
		}

		/**
		 * @param $id
		 */
		public function setId($id){
			$this->_id = $id;
		}

		/**
		 * @return mixed
		 */
		public function getEmail(){
			return $this->_email;
		}

		/**
		 * @param $email
		 */
		public function setEmail($email){
			$this->_email = $email;
		}

		/**
		 * @return mixed
		 */
		public function getFirstName(){
			return $this->_firstName;
		}

		/**
		 * @param $firstName
		 */
		public function setFirstName($firstName){
			$this->_firstName = $firstName;
		}

		/**
		 * @return mixed
		 */
		public function getLastName(){
			return $this->_lastName;
		}

		/**
		 * @param $lastName
		 */
		public function setLastName($lastName){
			$this->_lastName = $lastName;
		}

		/**
		 * @return mixed
		 */
		public function getRank(){
			return $this->_rank;
		}

		/**
		 * @param $rank
		 */
		public function setRank($rank){
			$this->_rank = $rank;
		}
	}