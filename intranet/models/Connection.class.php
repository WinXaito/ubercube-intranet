<?php
	/**
	 * Project: UberCube
	 */

	/**
	 * Class Connection
	 */
	class Connection{
		/**
		 * @var $_logged boolean
		 */
		private static $_logged;

		/**
		 * Redirige l'utilisateur vers la page de login s'il n'est pas connecté
		 */
		public static function redirectNoConnect(){
			Connection::init();

			if(!Connection::$_logged){
				require_once __DIR__.'/Redirection.class.php';
				Redirection::redirectLogin();
			}
		}

		/**
		 * @return bool
		 */
		public static function isLogged(){
			Connection::init();

			return Connection::$_logged;
		}

		/**
		 * Initialise la variable $_logged pour savoir si l'utilisateur est loggué ou non
		 */
		private static function init(){
			if(isset($_SESSION['user'])){
				Connection::$_logged = true;
			}else{
				Connection::$_logged = false;
			}
		}
	}