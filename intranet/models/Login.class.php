<?php
	/**
	 * Project: UberCube
	 */

	/**
	 * Cette classe nécéssite les classes suivantes:
	 * 	User.class.php
	 * 	UsersManager.class.php
	 */
	require_once __DIR__.'/User.class.php';
	require_once __DIR__.'/UsersManager.class.php';

	/**
	 * Class Login
	 */
	class Login{
		/**
		 * @var $_user User
		 * @var $_username String
		 * @var $_password String
		 * @var $_log String
		 * @var $_connectSuccess boolean
		 */

		private $_user;
		private $_username;
		private $_password;
		private $_log;
		private $_connectSuccess = false;

		/**
		 * Lance la procédure de login de l'utilisateur
		 */
		public function loginUser(){
			$usersManager = new UsersManager();
			$user = $usersManager->getUserByName($this->_username);

			if($user != null){
				if($user->getPassword() == sha1(USER_PASSWORD_SALTPREFIX.$this->_password.USER_PASSWORD_SALTSUFFIX)){
					$this->_user = $user;
					$this->createSession();
					$this->addLog('Connexion réussi');
					$this->_connectSuccess = true;
				}else{
					$this->addLog('Le nom d\'utilisateur ou le mot de passe entré est incorrecte');
				}
			}else{
				$this->addLog('Le nom d\'utilisateur inséré est invalide');
			}
		}

		/**
		 * Contrôle si les données $_POST sont toutes présentes
		 * @param array $post
		 * @param array $postData
		 *
		 * @return bool
		 */
		public function checkIssetPostData(Array $post, Array $postData){
			$boolOk = true;
			foreach($postData as $value){
				if(!isset($post[$value]))
					$boolOk = false;
			}
			return $boolOk;
		}

		/**
		 * Créer la session une fois le login effecuté
		 */
		private function createSession(){
			$_SESSION['user'] = [
				'id' => $this->_user->getId(),
				'name' => $this->_user->getUserName(),
				'email' => $this->_user->getEmail(),
				'firstName' => $this->_user->getFirstName(),
				'lastName' => $this->_user->getLastName(),
				'rank' => $this->_user->getRank()
			];
		}

		/**
		 * @return mixed
		 */
		public function getLog(){
			return $this->_log;
		}

		/**
		 * @param $message
		 */
		public function addLog($message){
			$this->_log .= '<p>'.$message.'</p>';
		}

		/**
		 * @param $username
		 */
		public function setUsername($username){
			$this->_username = $username;
		}

		/**
		 * @param $password
		 */
		public function setPassword($password){
			$this->_password = $password;
		}

		/**
		 * @return bool
		 */
		public function isConnectSuccess(){
			return $this->_connectSuccess;
		}
	}