<?php
	/**
	 * Project: UberCube
	 */

	/**
	 * Class Redirection
	 */
	class Redirection{
		/**
		 * Redirige vers l'url (Complète) passé en paramètre
		 * @param $path
		 */
		public static function redirect($path){
			header("Location:".$path);
		}

		/**
		 * Redirige vers la page de login
		 */
		public static function redirectLogin(){
			Redirection::redirect("/intranet/login");
		}

		/**
		 * Redirige vers la page de logout
		 */
		public static function redirectLogout(){
			Redirection::redirect("/intranet/logout");
		}

		/**
		 * Redirige vers la page d'inscription
		 */
		public static function redirectRegister(){
			Redirection::redirect("/intranet/register");
		}

		/**
		 * Redirige vers l'URL racine (Accueil)
		 */
		public static function redirectHome(){
			Redirection::redirect("/intranet/");
		}
	}