<?php
	/**
	 * Project: UberCube
	 */

	/**
	 * Cette classe nécéssite les classes suivantes:
	 * 	User.class.php
	 * 	UsersManager.class.php
	 */
	require_once __DIR__.'/User.class.php';
	require_once __DIR__.'/UsersManager.class.php';

	/**
	 * Class Register
	 */
	class Register{
		/**
		 * @var $_user User
		 * @var $_userManager UsersManager
		 * @var $_preset array
		 * @var $_log string
		 * @var $_checking boolean
		 * @var $_registred boolean
		 */
		private $_user;
		private $_userManager;
		private $_preset;
		private $_log;

		private $_checking = true;
		private $_registred = false;

		/**
		 * Constructeur
		 * Créer une instance de UserManager
		 */
		public function __construct(){
			$this->_userManager = new UsersManager();
		}

		/**
		 * @return User $_user
		 */
		public function getUser(){
			return $this->_user;
		}

		/**
		 * @param User $user
		 */
		public function setUser(User $user){
			$this->_user = $user;
		}

		/**
		 * @param $value
		 *
		 * @return mixed
		 */
		public function getPreset($value){
			return $this->_preset[$value];
		}

		/**
		 * @param array $preset
		 */
		public function setPreset(Array $preset){
			$this->_preset = $preset;
		}

		/**
		 * Effectue les contrôles sur les champs de l'inscription est inscrit l'utilisateur
		 */
		public function registreUser(){
			$this->checkUsername();
			$this->checkPassword();
			$this->checkEmail();
			$this->checkFisrtName();
			$this->checkLastName();

			if($this->_checking){
				$this->_userManager->createUser($this->_user);
				$this->_registred = true;
			}
		}

		/**
		 * Contrôle si toutes les données nécéssaire sont présentes
		 * @param array $post
		 * @param array $postData
		 *
		 * @return bool
		 */
		public function checkIssetPostData(Array $post, Array $postData){
			$boolOk = true;
			foreach($postData as $value){
				if(!isset($post[$value]))
					$boolOk = false;
			}
			return $boolOk;
		}

		/**
		 * Contrôle si tous les champs nécéssaire sont remplis
		 * @param array $post
		 * @param array $postData
		 *
		 * @return bool
		 */
		public function checkEmptyPostData(Array $post, Array $postData){
			$boolOk = true;
			foreach($postData as $value){
				if(empty($post[$value]))
					$boolOk = false;
			}

			return $boolOk;
		}

		/**
		 * @return mixed
		 */
		public function getLog(){
			return $this->_log;
		}

		/**
		 * @param $message
		 */
		public function addLog($message){
			$this->_log .= '<p>'.$message.'</p>';
		}

		/**
		 * @return bool
		 */
		public function isRegistred(){
			return $this->_registred;
		}

		/**
		 * Contrôle la validité du nom d'utilisateur
		 */
		private function checkUsername(){
			if($this->_userManager->getUserByName($this->_user->getUserName()) != null){
				$this->addLog('Le nom d\'utilisateur que vous avez choisissez à déjà été utilisé.');
				$this->_checking = false;
			}
		}

		/**
		 * Contrôle la validité du mot de passe
		 */
		private function checkPassword(){

		}

		/**
		 * Contrôle la validité de l'adresse email
		 */
		private function checkEmail(){

		}

		/**
		 * Contrôle si le prénom respecte les normes
		 */
		private function checkFisrtName(){

		}

		/**
		 * Contrôle si le nom respecte les normes
		 */
		private function checkLastName(){

		}
	}