<?php
	/**
	 * Project: UberCube
	 */

	/**
	 * Cette classe nécéssite les configurations
	 */
	require_once __DIR__.'/../config.php';

	/**
	 * Class Database
	 */
	class Database{
		/**
		 * @var $db PDO
		 */
		private static $db;

		/**
		 * @return mixed
		 */
		public static function getDatabase(){
			Database::init();
			return Database::$db;
		}

		/**
		 * Initialise la base de données si elle ne l'a pas encore été
		 */
		private static function init(){
			if(Database::$db == null){
				try{
					Database::$db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USERNAME, DB_PASSWORD);
				}catch(\Exception $e){
					die('Erreur : '.$e->getMessage());
				}
			}
		}
	}