<?php
	/**
	 * Project: UberCube
	 */

	/**
	 * Cette classe nécéssite la classe suivante:
	 * 	Databse.class.php
	 */
	require_once __DIR__.'/Database.class.php';

	/**
	 * Class UsersManager
	 */
	class UsersManager{
		/**
		 * Créer un utilisateur
		 * @param User $user
		 */
		public static function createUser(User $user){
			$db = Database::getDatabase();

			$req = $db->prepare("
				INSERT INTO users
				(username, password, email, firstName, lastName, rank)
				VALUES
				(:username, :password, :email, :firstName, :lastName, :rank)
			");
			$req->execute(array(
				'username' => $user->getUserName(),
				'password' => sha1(USER_PASSWORD_SALTPREFIX.$user->getPassword().USER_PASSWORD_SALTSUFFIX),
				'email' => $user->getEmail(),
				'firstName' => $user->getFirstName(),
				'lastName' => $user->getLastName(),
				'rank' => $user->getRank()
			));
		}

		/**
		 * Retourne un utilisateur selon son ID
		 * @param $id
		 *
		 * @return null
		 */
		public static function getUserById($id){
			$db = Database::getDatabase();

			$req = $db->prepare("
				SELECT *
				FROM users
				WHERE id = ?
			");
			$req->execute(array(
				$id,
			));

			$result = $req->fetch();

			if($result != null)
				return $result;
			else
				return null;
		}

		/**
		 * Retourne un utilisateur selon son Pseudo (Username)
		 * @param $username
		 *
		 * @return User
		 */
		public static function getUserByName($username){
			$db = Database::getDatabase();

			$req = $db->prepare("
				SELECT *
				FROM users
				WHERE username = ?
			");
			$req->execute(array(
				$username,
			));

			$result = $req->fetch();

			if($result != null){
				$user = new User();
				$user->setId($result['id']);
				$user->setUserName($result['username']);
				$user->setPassword($result['password']);
				$user->setEmail($result['email']);
				$user->setFirstName($result['firstName']);
				$user->setLastName($result['lastName']);
				$user->setRank($result['rank']);

				return $user;
			}else{
				return NULL;
			}
		}
	}