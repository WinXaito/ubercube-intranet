<?php
	/**
	 * Project: UberCube
	 */

	require_once __DIR__.'/../../config.php';
	require_once RACINE.'/models/Connection.class.php';
	Connection::redirectNoConnect();

	$content['title'] = 'Projet';
	$content['content-title'] = 'Projet';
	$content['content'] = include RACINE.'/views/project/project.html';

	$content['css'] = '<link rel="stylesheet" href="/intranet/assets/css/project.css"/>';

	require_once RACINE.'/views/default.php';