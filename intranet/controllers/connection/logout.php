<?php
	/**
	 * Project: UberCube
	 */

	require_once __DIR__.'/../../config.php';
	require_once RACINE.'/models/Connection.class.php';
	require_once RACINE.'/models/Redirection.class.php';

	if(!Connection::isLogged())
		Redirection::redirectLogin();

	session_destroy();
	unset($_SESSION);

	$content['title'] = 'Déconnexion';
	$content['content-title'] = 'Déconnexion';
	$content['content'] = include RACINE.'/views/connection/logout.html';

	$content['css'] = '<link rel="stylesheet" href="/intranet/assets/css/connection.css"/>';

	require_once RACINE.'/views/default.php';