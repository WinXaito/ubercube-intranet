<?php
	/**
	 * Project: UberCube
	 */

	require_once __DIR__.'/../../config.php';
	require_once RACINE.'/models/Login.class.php';
	require_once RACINE.'/models/Connection.class.php';
	require_once RACINE.'/models/Redirection.class.php';

	if(Connection::isLogged())
		Redirection::redirectHome();

	$login = new Login();
	$postData = ['username', 'password'];

	if($login->checkIssetPostData($_POST, $postData)){
		$login->setUsername($_POST['username']);
		$login->setPassword($_POST['password']);
		$login->loginUser();
	}

	$content['title'] = 'Connexion';
	$content['content-title'] = 'Connexion';
	$content['content'] = include RACINE.'/views/connection/login.html';

	$content['css'] = '<link rel="stylesheet" href="/intranet/assets/css/connection.css"/>';

	require_once RACINE.'/views/default.php';