<?php
	/**
	 * Project: UberCube
	 */

	require_once __DIR__.'/../../config.php';
	require_once RACINE.'/models/User.class.php';
	require_once RACINE.'/models/Register.class.php';
	require_once RACINE.'/models/Connection.class.php';
	require_once RACINE.'/models/Redirection.class.php';

	if(Connection::isLogged())
		Redirection::redirectHome();

	$register = new Register();

	$postIssetData = ['username', 'password', 'email', 'firstName', 'lastName'];
	$postEmptyData = ['username', 'password', 'email'];
	$preset = ['username' => '', 'email' => '', 'firstName' => '', 'lastName' => ''];
	$register->setPreset($preset);

	if($register->checkIssetPostData($_POST, $postIssetData) && $register->checkEmptyPostData($_POST, $postEmptyData)){
		$user = new User();
		$user->setUser($_POST['username'], $_POST['password'], $_POST['email'], $_POST['firstName'], $_POST['lastName'], 0);
		$register->setUser($user);
		$register->registreUser();
	}else{
		if($register->checkIssetPostData($_POST, $postIssetData)){
			$preset = ['username' => $_POST['username'], 'email' => $_POST['email'], 'firstName' => $_POST['firstName'], 'lastName' => $_POST['lastName']];
			$register->setPreset($preset);
		}
	}

	$content['title'] = 'Inscription';
	$content['content-title'] = 'Inscription';
	$content['content'] = include RACINE.'/views/connection/register.html';

	$content['css'] = '<link rel="stylesheet" href="/intranet/assets/css/connection.css"/>';

	require_once RACINE.'/views/default.php';