<?php
	/**
	 * Project: UberCube
	 */

	require_once __DIR__.'/../../config.php';
	require_once RACINE.'/models/Connection.class.php';
	Connection::redirectNoConnect();

	$content['title'] = $content['content-title'] = 'Accueil';
	$content['content'] = "Bienvenue sur UberCube";

	require_once RACINE.'/views/default.php';