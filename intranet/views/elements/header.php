<?php
	/**
	 * Project: UberCube
	 */

    require_once __DIR__.'/../../config.php';
    require_once RACINE.'/models/Connection.class.php';
?>

<?php if(Connection::isLogged()){ ?>
    <header>
        <div id="header-content">
            <h1><a href="/intranet/">UberCube Intranet</a></h1>

            <nav>
                <ul>
                    <li><a href="/intranet/" class="menu-link">Accueil</a></li>
                    <li><a href="/intranet/ubercube" class="menu-link">Ubercube</a></li>
                    <li><a href="/intranet/ubercube/members" class="menu-link">Membres</a></li>
                    <li><a href="" class="menu-link">Notifications</a></li>
                    <li class="dropdown">
                        <img src="https://cdn4.iconfinder.com/data/icons/wirecons-free-vector-icons/32/menu-alt-512.png" width="30px" height="35px"/>
                        <ul>
                            <li class="dropdown-item"><a href="/intranet/profile/WinXaito">Profil</a></li>
                            <li class="dropdown-item"><a href="/intranet/">Compte</a></li>
                            <li class="dropdown-item"><a href="/intranet/">Paramètres</a></li>
                            <li class="dropdown-item"><a href="/intranet/logout">Déconnexion</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
<?php }else{ ?>
    <header id="header-noconnect">
        <div id="header-content">
            <h1><a href="/intranet/">UberCube Intranet</a></h1>

            <nav>
                <ul>
                    <li><a href="/intranet/login" class="menu-link">Connexion</a></li>
                    <li><a href="/intranet/register" class="menu-link">Inscription</a></li>
                </ul>
            </nav>
        </div>
    </header>
<?php } ?>