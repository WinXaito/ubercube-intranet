<?php
	/**
	 * Project: UberCube
	 */

	if(isset($content)){
		if(!isset($content['title']))
			$content['title'] = '';
		if(!isset($content['content-title']))
			$content['content-title'] = '';
		if(!isset($content['content']))
			$content['content'] = '';
		if(!isset($content['css']))
			$content['css'] = '';
		if(!isset($content['js']))
			$content['js'] = '';
	}else{
		//Error 500
		exit();
	}
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="/intranet/assets/css/default.css">
		<?php echo $content['css']; ?>
		<title>UberCube - Intranet - <?php echo $content['title']; ?></title>
	</head>
	<body>
        <?php require_once __DIR__.'/elements/header.php'; ?>

        <section>
            <h2><?php echo $content['content-title']; ?></h2>

            <article>
                <?php echo $content['content']; ?>
            </article>
        </section>

        <script src="/intranet/assets/librairies/jquery.js"></script>
		<script src="/intranet/assets/js/default.js"></script>
		<?php echo $content['js']; ?>
	</body>
</html>