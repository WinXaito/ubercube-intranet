/**
 * Created by Kevin Vuilleumier on 20.01.2016.
 */

$(document).ready(function(){
    $('.menu-link').hover(function(){
        $(this).parent().css("border-bottom", "solid 5px #EF7F1A");
    },
    function(){
        $(this).parent().css("border-bottom", "");
    });

    $('.dropdown').hover(function(){
        $('.dropdown ul').css("display", "block");
    },
    function(){
        $('.dropdown ul').css("display", "none");
    });
});