#Intranet Ubercube

Intranet du projet Ubercube

Plus d'information sur le projet [ici](http://veridiangames.fr/fr/ubercube/).   
Le site est codé en PHP, utilisant un modèle MVC (Ce rapprochant d'un MVT).

Si vous souhaitez rejoindre le développement du site, n'hésiter pas à nous [contacter](mailto:support@winlap.ch).